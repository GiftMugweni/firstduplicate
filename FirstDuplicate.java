import java.util.Scanner;

public class FirstDuplicate
{
   public FirstDuplicate(){}
   
   public static void main(String[] args)
   {
      Scanner input = new Scanner(System.in);
      
      System.out.println("Enter values in the form a,b,c,d,... where a,b,c,d are integers");
      String[] values = input.nextLine().split(",");
      
      int[] test = new int[values.length];
      
      for(int i = 0; i < values.length; i++)
      {
         test[i] = Integer.parseInt(values[i]);
      }
      
      
      FirstDuplicate run = new FirstDuplicate();
      
      System.out.println("First Duplicate is: " + run.duplicate(test));
   }
   
   //check array for duplicate values
   public int duplicate(int[] test)
   {
      
      if(test.length > 1)
      {
         //get all the duplicates in array with their respective location
         Store[] duplicates = new Store[test.length];
         int pos = 0;
         for(int i = 0; i < test.length; i++)
         {
            for(int j = i + 1; j < test.length; j++)
            {
               if(test[i] == test[j])
               {
                  duplicates[pos] = new Store(test[j],j);
                  pos++;
                  break;
               }
            }
         }
         
         //find duplicate with the smallest reference location
         Store ans = new Store(-1,-1);
         
         for(Store obj : duplicates)
         {
            if(obj != null)
            {
               if(ans.ref == -1)
               {
                  ans.set(obj);
                  continue;
               }
               
               if(obj.lessThan(ans))
               {
                  ans.set(obj);
               }
            }
         }
         
         return ans.value;
      }
      
      return -1;
   }
   
   //Helper class to keep track of both the value and reference location
   private class Store
   {
      private int value;
      private int ref;
      
      public Store(int value,int  ref)
      {
         this.value = value;
         this.ref = ref;
      }
      
      public boolean lessThan(Store other)
      {
         return ref < other.ref;
      }
      
      public void set(int value,int ref)
      {
         this.value = value;
         this.ref = ref;
      }
      
      public void set(Store other)
      {
         value = other.value;
         ref = other.ref;
      }
      
      public String toString()
      {
         return "value: " + value + " index: "+ ref;
      }
      
      public boolean equals(Object other)
      {
         if(other == null)
            return false;
         
         if(other instanceof Store)
         {
            Store cc = (Store)other;
            return value == cc.value
               && ref == cc.ref;
         }
         
         return false;
      }
      
   }
}